# DBS Coding Exercise 

## Prerequisites

1. Install JDK from [https://www.oracle.com/java/technologies/javase-jdk11-downloads.html](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html) and setup environment path varibles

2. Install Maven from [https://maven.apache.org/install.html](https://maven.apache.org/install.html)
and setup environment path varibles

3. Install Docker Desktop [https://www.docker.com/products/docker-desktop](https://www.docker.com/products/docker-desktop) and setup environment path varibles

## Build And Run

1.Run following command to build project
   
    mvn clean install
    
2.Run following command to setup kafka

	docker-compose up
3.Run following command to start application 

	java -jar ./target/dbs-coding-exercise-0.0.1-SNAPSHOT.jar

4.Open following URL from the browser to view application 

	http://localhost:8080/