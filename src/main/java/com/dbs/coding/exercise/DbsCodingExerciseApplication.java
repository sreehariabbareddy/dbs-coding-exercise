package com.dbs.coding.exercise;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DbsCodingExerciseApplication {
	public static void main(String[] args) {
		SpringApplication.run(DbsCodingExerciseApplication.class, args);
	}

}
