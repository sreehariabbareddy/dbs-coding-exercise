package com.dbs.coding.exercise.messaging;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import com.dbs.coding.exercise.domain.model.StockPrice;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class Producer {
	private static final String TOPIC = "test_topic";
	@Autowired
	private KafkaTemplate<String, String> kafkaTemplate;

	@Autowired
	private ObjectMapper mapper;

	public void sendMessage(StockPrice stockPrice) {

		try {
			String stockPriceJson = mapper.writeValueAsString(stockPrice);
			this.kafkaTemplate.send(TOPIC, stockPriceJson);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Bean
	public NewTopic createTopic() {

		return new NewTopic(TOPIC, 3, (short) 1);
	}

}
