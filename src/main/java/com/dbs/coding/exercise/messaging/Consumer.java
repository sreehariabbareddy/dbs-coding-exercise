package com.dbs.coding.exercise.messaging;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.dbs.coding.exercise.data.service.StockDataService;
import com.dbs.coding.exercise.domain.model.StockPrice;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class Consumer {

	@Autowired
	private StockDataService stockDataService;
	
	@Autowired
	private ObjectMapper mapper;

	@KafkaListener(topics = "test_topic", groupId = "group_id")
	public void consumeMessage(String stockPriceJson) {
		System.out.println(stockPriceJson);
		try {
			StockPrice stockPrice = mapper.readValue(stockPriceJson, StockPrice.class);
			stockDataService.updateStockPrice(stockPrice);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
