package com.dbs.coding.exercise.data.service;

import java.util.List;

import com.dbs.coding.exercise.domain.model.Stock;
import com.dbs.coding.exercise.domain.model.StockPrice;

public interface StockDataService {
	public void updateStockPrice(StockPrice stockPrice);

	public List<Stock> listStocks();
}
