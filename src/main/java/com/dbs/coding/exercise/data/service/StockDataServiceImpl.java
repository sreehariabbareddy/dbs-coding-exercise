package com.dbs.coding.exercise.data.service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.dbs.coding.exercise.domain.model.Stock;
import com.dbs.coding.exercise.domain.model.StockPrice;

@Service
public class StockDataServiceImpl implements StockDataService {
	private Map<String, Stock> stockData = new LinkedHashMap<>();

	public void updateStockPrice(StockPrice stockPrice) {

		if (stockPrice == null || stockPrice.getSymbol() == null)
			throw new RuntimeException("Invalid Stock Symbol");

		double price = (stockPrice.getBidPrice() + stockPrice.getAskPrice()) / 2;
		String trend = "";

		String key = stockPrice.getSymbol();
		Stock stock = stockData.get(key);
		if (stock == null) {
			stock = new Stock();
			stock.setSymbol(stockPrice.getSymbol());

		} else {
			trend = "Down";
			if (price > stock.getPrice())
				trend = "Up";
		}

		if (stockPrice.getEventTime() > stock.getTime()) {
			stock.setTrend(trend);
			stock.setPrice(price);
			stock.setTime(stockPrice.getEventTime());
			stockData.put(key, stock);
		}

	}

	public List<Stock> listStocks() {
		List<Stock> quoteList = new ArrayList<>(stockData.values());
		return quoteList;
	}
}
