package com.dbs.coding.exercise.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class WelcomePageController {

	@Autowired
	public WelcomePageController() {
	}

	@GetMapping("/")
	public String index() {
		return "index";
	}

}
