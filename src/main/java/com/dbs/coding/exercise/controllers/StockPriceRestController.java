package com.dbs.coding.exercise.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.dbs.coding.exercise.data.service.StockDataService;
import com.dbs.coding.exercise.domain.model.Stock;
import com.dbs.coding.exercise.domain.model.StockPrice;
import com.dbs.coding.exercise.messaging.Producer;

@RestController
public class StockPriceRestController {

	
	private final Producer producer;

	@Autowired
	private StockDataService stockDataService;

	@Autowired
	public StockPriceRestController(Producer producer) {
		this.producer = producer;
	}

	@PostMapping("/stock/price")
	public void stockPriceUpdate(@RequestBody StockPrice stock) {
		this.producer.sendMessage(stock);

	}

	@GetMapping("/stock/price/list")
	public @ResponseBody List<Stock> listStockPrice() {
		return stockDataService.listStocks();

	}
}
