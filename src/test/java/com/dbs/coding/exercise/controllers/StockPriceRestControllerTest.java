package com.dbs.coding.exercise.controllers;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.dbs.coding.exercise.data.service.StockDataService;
import com.dbs.coding.exercise.domain.model.Stock;
import com.dbs.coding.exercise.domain.model.StockPrice;
import com.dbs.coding.exercise.messaging.Producer;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@WebMvcTest(value = StockPriceRestController.class)
public class StockPriceRestControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private Producer producer;

	@MockBean
	private StockDataService stockDataService;

	@Test
	public void testStockPriceUpdate() throws Exception {

		// Given

		Mockito.doNothing().when(producer).sendMessage(Mockito.any(StockPrice.class));

		StockPrice stockPrice = new StockPrice();
		stockPrice.setSymbol("D05SGX");
		stockPrice.setBidPrice(20.1);
		stockPrice.setAskPrice(20.2);
		stockPrice.setEventTime(System.currentTimeMillis());
		ObjectMapper mapper = new ObjectMapper();
		String stockPriceJson = mapper.writeValueAsString(stockPrice);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/stock/price").accept(MediaType.APPLICATION_JSON)
				.content(stockPriceJson).contentType(MediaType.APPLICATION_JSON);

		// When
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		// Then
		MockHttpServletResponse response = result.getResponse();
		assertEquals(HttpStatus.OK.value(), response.getStatus());

	}

	@Test
	public void testListStockPrice() throws Exception {

		// Given

		Stock stock = new Stock();
		stock.setSymbol("D05SGX");
		stock.setPrice(20.2);
		stock.setTrend("Up");

		List<Stock> stocks = new ArrayList<Stock>();
		stocks.add(stock);
		Mockito.when(stockDataService.listStocks()).thenReturn(stocks);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/stock/price/list")
				.accept(MediaType.APPLICATION_JSON);

		// When
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		ObjectMapper mapper = new ObjectMapper();
		String expected = mapper.writeValueAsString(stocks);
		System.out.println(result.getResponse());

		// Then
		assertEquals(expected, result.getResponse().getContentAsString());

	}
}
